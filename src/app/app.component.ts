import { Component } from '@angular/core';
import { AgentFacade } from './features/agent/service/agent.facade';
import { AgentConfig, ExtDataSource, DataTrigger } from './features/agent/store/agent.model';
import { MindsphereStandardEvent } from '@mindconnect/mindconnect-nodejs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  dataSourceConfiguration$ = this.agentFacade.dataSourceConfiguration$;
  dataSources$ = this.agentFacade.dataSources$;
  isOnboarded$ = this.agentFacade.isOnboarded$;
  error$ = this.agentFacade.error$;
  nextTriggerError$ = this.agentFacade.nextTriggerError$;

  showOnboardingModal = false;
  showEventModal = false;
  showTriggerModal = false;

  selectedDataSource = null;

  constructor(private agentFacade: AgentFacade) {
  }

  onOnboard(agentConfig: AgentConfig) {
    this.agentFacade.setConfig(agentConfig);
    this.agentFacade.onboard();
  }

  onOffboard() {
    this.agentFacade.offboard();
  }

  onSendEvent(event: MindsphereStandardEvent) {
    this.agentFacade.sendEvent(event);
  }

  onSaveDataTrigger(dataTrigger: DataTrigger) {
    this.agentFacade.saveDataTriggerEvent(this.selectedDataSource, dataTrigger);
  }

  onDismissError() {
    this.agentFacade.removeNextTriggerError();
  }
}
