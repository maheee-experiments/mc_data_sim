import { Injectable } from '@angular/core';
import { DataPoint, TimeStampedDataPoint } from '@mindconnect/mindconnect-nodejs';
import { ExtDataPoint, DataTrigger } from '../store/agent.model';
import { SEND_INTERVAL_IN_S } from '../agent.constants';

@Injectable()
export class DataCreatorService {

  private createTimeStampedDataPointList(ts: number, dataPoints: any[]) {
    return {
      timestamp: (new Date(ts)).toISOString(),
      values: dataPoints
    };
  }

  private createDataPoint(dataPoint: ExtDataPoint, value: number) {
    return {
      dataPointId: '' + dataPoint.id,
      qualityCode: "0",
      value: '' + value
    };
  }

  private calcValue(secs: number, t: DataTrigger) {
    const rad = ((Math.PI * 2) * (t.frequency / 1000)) * secs;
    const range = t.upperBound - t.lowerBound;
    switch (t.func) {
      case 'const':
        return t.lowerBound + (range / 2);
      case 'rand':
        return t.lowerBound + (Math.random() * range);
      case 'sine':
        return t.lowerBound + (((Math.sin(rad) + 1) / 2) * range);
      case 'cosine':
        return t.lowerBound + (((Math.cos(rad) + 1) / 2) * range);
      case 'square':
        return t.lowerBound + (Math.round((Math.sin(rad) + 1) / 2) * range);
    }
  }

  private prepareValue(dataPoint: ExtDataPoint, value) {
    if ('' + dataPoint.type === 'INT') {
      return Math.round(value);
    }
    return value;
  }

  private createResultingData(results: Map<number, any[]>) {
    const result = [];
    for (let [key, value] of results) {
      result.push(this.createTimeStampedDataPointList(key, value));
    }
    return result;
  }

  private addToMapList(results: Map<number, any[]>, ts, dp) {
    let list = results.get(ts);
    if (!list) {
      list = [];
      results.set(ts, list);
    }
    list.push(dp);
  }

  createBulkData(intervalNo: number, dataPoints: ExtDataPoint[]): TimeStampedDataPoint[] {
    const now: number = Date.now();
    const seconds: number = intervalNo * SEND_INTERVAL_IN_S;

    const results: Map<number, any[]> = new Map();

    dataPoints.forEach(dataPoint => {
      const t = dataPoint.dataTrigger;
      const amountInInterval = t.testFrequency * SEND_INTERVAL_IN_S;

      for (let i = 0; i < amountInInterval; ++i) {
        const loopSecondsToAdd = ((SEND_INTERVAL_IN_S / amountInInterval) * i);
        const loopSeconds = seconds + loopSecondsToAdd;
        const loopNow = now - (SEND_INTERVAL_IN_S * 1000) + (loopSecondsToAdd * 1000);

        this.addToMapList(results, loopNow,
          this.createDataPoint(dataPoint, this.prepareValue(dataPoint, this.calcValue(loopSeconds, t)))
        );
      }
    });

    return this.createResultingData(results);
  }
}
