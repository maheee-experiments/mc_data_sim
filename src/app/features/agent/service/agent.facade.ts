import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { AgentState } from '../store/agent.reducer';
import { AgentConfig, ExtDataSource, ExtDataPoint, DataTrigger } from '../store/agent.model';
import * as actions from '../store/agent.actions';
import * as selector from '../store/agent.selector';
import { Observable } from 'rxjs';
import { DataSourceConfiguration, MindsphereStandardEvent } from '@mindconnect/mindconnect-nodejs';

@Injectable()
export class AgentFacade {

  dataSourceConfiguration$: Observable<DataSourceConfiguration> = this.store.select(selector.dataSourceConfiguration);
  dataSources$: Observable<ExtDataSource[]> = this.store.select(selector.dataSources);
  isOnboarded$: Observable<boolean> = this.store.select(selector.isOnboarded);
  triggerErrors$: Observable<Error[]> = this.store.select(selector.triggerErrors);
  nextTriggerError$: Observable<Error> = this.store.select(selector.nextTriggerError);

  error$: Observable<Error> = this.store.select(selector.error);

  constructor(private store: Store<AgentState>) {
  }

  setConfig(config: AgentConfig) {
    this.store.dispatch(actions.setConfig({config}));
  }

  onboard() {
    this.store.dispatch(actions.onboard());
  }

  offboard() {
    this.store.dispatch(actions.offboard());
  }

  sendEvent(event: MindsphereStandardEvent) {
    this.store.dispatch(actions.sendEvent({event}));
  }

  saveDataTriggerEvent(dataPoint: ExtDataPoint, dataTrigger: DataTrigger) {
    this.store.dispatch(actions.saveDataTrigger({dataPoint, dataTrigger}));
  }

  removeNextTriggerError() {
    this.store.dispatch(actions.removeTriggerError({pos: 0}));
  }
}
