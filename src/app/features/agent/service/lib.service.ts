import { Injectable } from '@angular/core';
import { AgentConfig } from '../store/agent.model';
import { MindConnectAgent } from '@mindconnect/mindconnect-nodejs';
import { Observable, of, from } from 'rxjs';
import { switchMap, map } from 'rxjs/operators';
import { EVENT_SOURCE_TYPE, EVENT_SOURCE_ID, EVENT_SOURCE } from '../agent.constants';

declare var mc;

@Injectable()
export class LibService {

  createAgent(config: AgentConfig): MindConnectAgent {
    return new mc.MindConnectAgent(config);
  }

  onboard(agent: MindConnectAgent): Observable<MindConnectAgent> {
    return of({}).pipe(
      map(() => agent.IsOnBoarded()),
      switchMap(isOnboarded => !isOnboarded ? from(agent.OnBoard()) : of(null)),
      map(() => agent.HasDataSourceConfiguration()),
      switchMap(hasConfig => !hasConfig ? from(agent.GetDataSourceConfiguration()) : of(null)),
      map(() => agent)
    );
  }
  
  postEvent(agent: MindConnectAgent, event) {
    return from(agent.PostEvent({
      ...event,
      entityId: agent.ClientId(),
      sourceType: EVENT_SOURCE_TYPE,
      sourceId: EVENT_SOURCE_ID,
      source: EVENT_SOURCE
    }));
  }

  sendBulkData(agent: MindConnectAgent, bulkData) {
    return from(agent.BulkPostData(bulkData));
  }
}

