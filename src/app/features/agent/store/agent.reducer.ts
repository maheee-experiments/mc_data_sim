import { createReducer, on } from '@ngrx/store';
import * as actions from './agent.actions';
import { AgentConfig, DataTrigger } from './agent.model';
import { MindConnectAgent } from '@mindconnect/mindconnect-nodejs';
 
export interface AgentState {
  config: AgentConfig;
  agent: MindConnectAgent;
  error: Error;
  triggerErrors: Error[];
  dataTrigger: {[id: string]: DataTrigger};
}

export const initialState: AgentState = {
  config: null,
  agent: null,
  error: null,
  triggerErrors: [],
  dataTrigger: {}
};
 
const _agentReducer = createReducer(initialState,
  on(actions.setConfig, (state, action) => ({...state, config: action.config, error: null})),
  on(actions.setAgent, (state, action) => ({...state, agent: action.agent, error: null})),
  on(actions.setError, (state, action) => ({...state, error: action.error, config: null, agent: null})),
  on(actions.addTriggerError, (state, action) => ({...state, triggerErrors: [...state.triggerErrors, action.error]})),
  on(actions.removeTriggerError, (state, action) => {
    if (state.triggerErrors && action.pos >= 0 && action.pos < state.triggerErrors.length) {
      let triggerErrors = [...state.triggerErrors];
      triggerErrors.splice(action.pos, 1)
      return {
        ...state,
        triggerErrors
      }
    }
    return state;
  }),
  on(actions.clearTriggerErrors, (state, action) => ({...state, triggerErrors: []})),
  on(actions.offboard, (state, action) => ({...state, error: null, config: null, agent: null})),
  on(actions.saveDataTrigger, (state, action) => ({
    ...state,
    dataTrigger: {
      ...state.dataTrigger,
      [action.dataPoint.id]: action.dataTrigger
    }
  }))
);
 
export function agentReducer(state, action) {
  const newstate = _agentReducer(state, action);
  // console.log(action, newstate);
  return newstate;
}
