import { DataSource, DataPoint } from '@mindconnect/mindconnect-nodejs';

export interface AgentConfigDetails {
  baseUrl: string;
  iat: string;
  clientCredentialProfile: string[];
  clientId: string;
  tenant: string;
}

export interface AgentConfig {
  content: AgentConfigDetails;
  expiration: string;
}

export interface DataTrigger {
  lowerBound: number;
  upperBound: number;
  frequency: number;
  func: string;
  testFrequency: number;
}

export interface ExtDataPoint extends DataPoint {
  dataTrigger?: DataTrigger;
}

export interface ExtDataSource extends DataSource {
  dataPoints: Array<ExtDataPoint>;
}
