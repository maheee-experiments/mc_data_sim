import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of, interval, forkJoin } from 'rxjs';
import { mergeMap, map, switchMap, take, tap, catchError } from 'rxjs/operators';
import * as actions from './agent.actions';
import { AgentState } from './agent.reducer';
import { Store } from '@ngrx/store';
import * as selector from './agent.selector';
import { LibService } from '../service/lib.service';
import { DataCreatorService } from '../service/data-creator.service';
import { SEND_INTERVAL_IN_S } from '../agent.constants';
 
@Injectable()
export class AgentEffects {
 
  onboard$ = createEffect(() => this.actions$.pipe(
    ofType(actions.onboard),
    switchMap(() => 
      this.store.select(selector.config).pipe(
        take(1),
        map(config => this.libService.createAgent(config)),
        mergeMap(agent => this.libService.onboard(agent)),
        tap(agent => { if (!agent) throw new Error('Invalid Agent') }),
        map(agent => actions.setAgent({agent})),
        catchError(error => of(actions.setError({error})))
      )
    ),
  ));

  sendEvent$ = createEffect(() => this.actions$.pipe(
    ofType(actions.sendEvent),
    switchMap(action => 
      this.store.select(selector.agent).pipe(
        take(1),
        mergeMap(agent => this.libService.postEvent(agent, action.event)),
        tap(success => { if (!success) throw new Error('Sending event failed') }),
        map(() => actions.noop()),
        catchError(error => of(actions.addTriggerError({error})))
      )
    ),
  ));

  sendData$ = createEffect(() => interval(SEND_INTERVAL_IN_S * 1000).pipe(
    switchMap((no) => forkJoin(
      of(no),
      this.store.select(selector.agent).pipe(take(1)),
      this.store.select(selector.dataPointsWithDataTrigger).pipe(take(1))
    )),
    switchMap(([no, agent, dataPoints]) => {
      if (!agent || !dataPoints || !dataPoints.length) {
        return of(actions.noop());
      }
      return this.libService.sendBulkData(agent, this.dataCreatorService.createBulkData(no, dataPoints)).pipe(
        map(() => actions.noop()),
        catchError(error => of(actions.addTriggerError({error})))
      )
    })
  ));
 
  constructor(
    private actions$: Actions,
    private store: Store<AgentState>,
    private libService: LibService,
    private dataCreatorService: DataCreatorService
  ) {
  }
}
