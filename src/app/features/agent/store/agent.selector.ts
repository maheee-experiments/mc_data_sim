import { createSelector, createFeatureSelector } from '@ngrx/store';
import { AGENT_FEATURE_NAME } from '../agent.constants';
import { AgentState } from './agent.reducer';
import { DataSourceConfiguration } from '@mindconnect/mindconnect-nodejs';

export const agentFeatureState = createFeatureSelector<AgentState>(AGENT_FEATURE_NAME);

export const config = createSelector(agentFeatureState, (state) => state.config);
export const agent = createSelector(agentFeatureState, (state) => state.agent);
export const error = createSelector(agentFeatureState, (state) => state.error);
export const triggerErrors = createSelector(agentFeatureState, (state) => state.triggerErrors);
export const dataTrigger = createSelector(agentFeatureState, (state) => state.dataTrigger);

export const nextTriggerError = createSelector(triggerErrors, triggerErrors =>
  triggerErrors && triggerErrors.length ? triggerErrors[0] : null);

export const dataSourceConfiguration = createSelector(agent, (agent: any) => 
  <DataSourceConfiguration>agent?._configuration?.dataSourceConfiguration);

export const dataSources = createSelector(dataSourceConfiguration, dataTrigger, (dataSourceConfiguration, dataTrigger) => {
  const sources = dataSourceConfiguration?.dataSources || [];

  return sources.map(source => ({
    ...source,
    dataPoints: source.dataPoints.map(point => ({
      ...point,
      dataTrigger: dataTrigger[point.id]
    }))
  }));
});

export const dataPoints = createSelector(dataSources, dataSources => dataSources.reduce((prev, curr) => prev.concat(curr.dataPoints), []));

export const dataPointsWithDataTrigger = createSelector(dataPoints, dataPoints => 
  dataPoints.filter(dataPoint => !!dataPoint.dataTrigger));


export const isOnboarded = createSelector(agent, agent => !!(agent && agent.IsOnBoarded()));

