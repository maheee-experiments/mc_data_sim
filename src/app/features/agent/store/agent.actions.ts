import { createAction, props } from '@ngrx/store';
import { AgentConfig, ExtDataPoint, DataTrigger } from './agent.model';
import { MindConnectAgent, MindsphereStandardEvent } from '@mindconnect/mindconnect-nodejs';

export const noop = createAction('[Agent] Noop');

export const setConfig = createAction(
  '[Agent] Set Config',
  props<{config: AgentConfig}>());

export const setAgent = createAction(
  '[Agent] Set Agent',
  props<{agent: MindConnectAgent}>());

export const setError = createAction(
  '[Agent] Set Error',
  props<{error: Error}>());

export const addTriggerError = createAction(
  '[Agent] Add Trigger Error',
  props<{error: Error}>());

export const removeTriggerError = createAction(
  '[Agent] Remove Trigger Error',
  props<{pos: number}>());

export const clearTriggerErrors = createAction('[Agent] Clear Trigger Errors');

export const onboard = createAction('[Agent] Onboard');
export const offboard = createAction('[Agent] Offboard');

export const sendEvent = createAction(
  '[Agent] Send Event',
  props<{event: MindsphereStandardEvent}>());

export const saveDataTrigger = createAction(
  '[Agent] Save Data Trigger',
  props<{dataPoint: ExtDataPoint, dataTrigger: DataTrigger}>());
