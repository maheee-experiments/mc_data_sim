export const AGENT_FEATURE_NAME = 'agent';

export const EVENT_SOURCE_TYPE = 'Event';
export const EVENT_SOURCE_ID = 'application';
export const EVENT_SOURCE = 'McDataSim';

export const SEND_INTERVAL_IN_S = 5; // seconds
