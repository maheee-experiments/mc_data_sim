import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { agentReducer } from './store/agent.reducer';
import { AgentEffects } from './store/agent.effects';
import { AgentFacade } from './service/agent.facade';
import { AGENT_FEATURE_NAME } from './agent.constants';
import { LibService } from './service/lib.service';
import { DataCreatorService } from './service/data-creator.service';
 
@NgModule({
  imports: [
    BrowserModule,
    StoreModule.forFeature(AGENT_FEATURE_NAME, agentReducer),
    EffectsModule.forFeature([AgentEffects])
  ],
  declarations: [],
  providers: [
    AgentFacade,
    LibService,
    DataCreatorService
  ],
  exports: []
})
export class AgentModule {
}
