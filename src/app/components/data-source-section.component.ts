import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ExtDataSource, ExtDataPoint } from '../features/agent/store/agent.model';

@Component({
  selector: 'app-data-source-section',
  templateUrl: './data-source-section.component.html'
})
export class DataSourceSectionComponent {
  @Input() dataSource: ExtDataSource;
  @Output() sendEvent: EventEmitter<void> = new EventEmitter();
  @Output() dataPointYesClick: EventEmitter<ExtDataPoint> = new EventEmitter();
  @Output() dataPointNoClick: EventEmitter<ExtDataPoint> = new EventEmitter();
}
