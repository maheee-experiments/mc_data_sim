import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  @Input() isOnboarded;
  @Input() isWorking = false;
  @Input() nextError: string;

  @Output() onboard: EventEmitter<void> = new EventEmitter();
  @Output() offboard: EventEmitter<void> = new EventEmitter();
  @Output() dismissError: EventEmitter<void> = new EventEmitter();

  menuOpen = false;

  onBurgerClicked() {
    this.menuOpen = !this.menuOpen;
  }
}
