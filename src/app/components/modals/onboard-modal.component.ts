import { Component, Output, EventEmitter } from '@angular/core';
import { AgentConfig } from '../../features/agent/store/agent.model';

@Component({
  selector: 'app-onboard-modal',
  templateUrl: './onboard-modal.component.html'
})
export class OnboardModalComponent {
  @Output() onboard: EventEmitter<AgentConfig> = new EventEmitter();
  @Output() cancel: EventEmitter<void> = new EventEmitter();

  hasJsonError = false;

  onOnboardClicked(configString) {
    try {
      this.hasJsonError = false;
      const config: AgentConfig = JSON.parse(configString);
      this.onboard.emit(config);
    } catch (e) {
      this.hasJsonError = true;
    }
  }
}
