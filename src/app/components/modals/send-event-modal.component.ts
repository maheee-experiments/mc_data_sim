import { Component, Output, EventEmitter, Input } from '@angular/core';
import { AgentConfig } from '../../features/agent/store/agent.model';
import { MindsphereStandardEvent } from '@mindconnect/mindconnect-nodejs';

@Component({
  selector: 'app-send-event-modal',
  templateUrl: './send-event-modal.component.html'
})
export class SendEventModalComponent {
  @Output() sendEvent: EventEmitter<MindsphereStandardEvent> = new EventEmitter();
  @Output() cancel: EventEmitter<void> = new EventEmitter();

  onSendClicked(severity: number, description: string) {
    this.sendEvent.emit({
      entityId: '',
      severity: severity * 1,
      timestamp: new Date().toISOString(),
      description
    });
  }
}
