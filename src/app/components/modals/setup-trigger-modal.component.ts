import { Component, Output, EventEmitter } from '@angular/core';
import { DataTrigger } from 'src/app/features/agent/store/agent.model';

@Component({
  selector: 'app-setup-trigger-modal',
  templateUrl: './setup-trigger-modal.component.html',
  styleUrls: ['./setup-trigger-modal.component.scss']
})
export class SetupTriggerModalComponent {
  @Output() saveDataTrigger: EventEmitter<DataTrigger> = new EventEmitter();
  @Output() cancel: EventEmitter<void> = new EventEmitter();

  hasLowerBoundError = false;
  hasUpperBoundError = false;
  hasFrequencyError = false;

  private clearErrors() {
    this.hasLowerBoundError = false;
    this.hasUpperBoundError = false;
    this.hasFrequencyError = false;
  }

  onSaveClicked(lowerBound: string, upperBound: string, frequency: string, func: string, testFrequency: string) {
    this.clearErrors();

    const lowerBoundNo = Number.parseFloat(lowerBound);
    const upperBoundNo = Number.parseFloat(upperBound);
    const frequencyNo = Number.parseFloat(frequency);

    this.hasLowerBoundError = Number.isNaN(lowerBoundNo);
    this.hasUpperBoundError = Number.isNaN(upperBoundNo);
    this.hasFrequencyError = Number.isNaN(frequencyNo);

    if (!this.hasLowerBoundError && !this.hasUpperBoundError && !this.hasFrequencyError) {
      this.saveDataTrigger.emit({
        lowerBound: lowerBoundNo,
        upperBound: upperBoundNo,
        frequency: frequencyNo,
        func,
        testFrequency: Number.parseFloat(testFrequency)
      });
    }
  }
}
