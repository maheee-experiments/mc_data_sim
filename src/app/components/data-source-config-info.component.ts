import { Component, Input } from '@angular/core';
import { DataSourceConfiguration } from '@mindconnect/mindconnect-nodejs';

@Component({
  selector: 'app-data-source-config-info',
  templateUrl: './data-source-config-info.component.html'
})
export class DataSourceConfigInfoComponent {
  @Input() dataSourceConfiguration: DataSourceConfiguration;
}
