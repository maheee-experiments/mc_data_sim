import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ExtDataPoint } from '../features/agent/store/agent.model';

@Component({
  selector: 'app-data-point-table',
  templateUrl: './data-point-table.component.html'
})
export class DataPointTableComponent {
  @Input() dataPoints: ExtDataPoint[];
  @Output() yesClick: EventEmitter<ExtDataPoint> = new EventEmitter();
  @Output() noClick: EventEmitter<ExtDataPoint> = new EventEmitter();
}
