import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { AppComponent } from './app.component';
import { AgentModule } from './features/agent/agent.module';
import { HeaderComponent } from './components/header.component';
import { DataPointTableComponent } from './components/data-point-table.component';
import { DataSourceConfigInfoComponent } from './components/data-source-config-info.component';
import { OnboardModalComponent } from './components/modals/onboard-modal.component';
import { DataSourceSectionComponent } from './components/data-source-section.component';
import { SendEventModalComponent } from './components/modals/send-event-modal.component';
import { SetupTriggerModalComponent } from './components/modals/setup-trigger-modal.component';

@NgModule({
  imports: [
    BrowserModule,
    StoreModule.forRoot({}),
    EffectsModule.forRoot([]),
    AgentModule
  ],
  declarations: [
    AppComponent,
    HeaderComponent,
    DataPointTableComponent,
    DataSourceConfigInfoComponent,
    OnboardModalComponent,
    DataSourceSectionComponent,
    SendEventModalComponent,
    SetupTriggerModalComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
 