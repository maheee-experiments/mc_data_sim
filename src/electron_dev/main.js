const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
//const Menu = electron.Menu;

const path = require('path');
const url = require('url');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;

function createWindow () {
  mainWindow = new BrowserWindow({width: 1200, height: 600, webPreferences: {
    nodeIntegration: true
  }});

  mainWindow.loadURL('http://localhost:4200/');
  mainWindow.webContents.openDevTools();

  //Menu.setApplicationMenu(null);

  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
  });
}

app.on('ready', createWindow);
app.on('window-all-closed', function () {
  app.quit()
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
